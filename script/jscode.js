function createNewUser() {

  let newFirstName = prompt('Write your name pls', 'Joe');
  let newLastName = prompt('Write your Surname pls', 'Satriani');

  let newUser = {
    firstName: newFirstName,
    lastName: newLastName,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase()
    },
    setFirstName: function(value) {
      Object.defineProperty(this, 'firstName', { writable: true });
      this.firstName = value;
      Object.defineProperty(this, 'firstName', { writable: false });
    },
    setLastName: function(value) {
      Object.defineProperty(this, 'lastName', { writable: true });
      this.lastName = value;
      Object.defineProperty(this, 'lastName', { writable: false });
    }
  }
  Object.defineProperty(newUser, 'firstName', { writable: false });
  Object.defineProperty(newUser, 'lastName', { writable: false });
  return newUser;
}
console.log('createNewUser().getLogin()', createNewUser().getLogin());